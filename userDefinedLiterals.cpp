#include<iostream>
#include<ostream>

namespace Distance{
    class MyDistance{
        public:
        MyDistance(double i):m(i){}
        
        //function for addition operation
        friend MyDistance operator +(const MyDistance &a,const MyDistance &b){
            return MyDistance(a.m+b.m);
        }
        //function to display in the required unit
        friend std::ostream& operator<< (std::ostream &out,const MyDistance& Dist){
            out<<Dist.m<<" m";
            return out;
        } 
        private:
            double m;
    };
   
    namespace Unit{
    //convert km to meter
    MyDistance operator "" _km(long double d){
      return MyDistance(1000*d);
    }
    //display in meter
    MyDistance operator "" _m(long double m){
      return MyDistance(m);
    }
    //convert decimeter to meter
    MyDistance operator "" _dm(long double d){
      return MyDistance(d/10);
    }
    //convert centimeter to meter
    MyDistance operator "" _cm(long double c){
      return MyDistance(c/100);
    }
  }
}

using namespace Distance::Unit;

int main(){

  std:: cout << std::endl;
  //convert all units to meter
  std::cout << "1.0_km: " << 1.0_km << std::endl;
  std::cout << "1.0_m: " << 1.0_m << std::endl;
  
  std::cout << std::endl;
  //Addition of distance and display in meters
  std::cout << "1.0_km + 2.0_dm +  3.0_dm + 4.0_cm: " << 1.0_km + 2.0_dm +  3.0_dm + 4.0_cm << std::endl;
  std::cout << std::endl;
}