#include<iostream>

int main(){
    int i = 20; // A variable containing an integer
  int* iptr = &i; // A pointer that points to 'i'
  std::cout << iptr << std::endl; // Accessing the address stored in the pointer
  std::cout << *iptr << std::endl; // Accessing the value that the pointer points to
  i = 30;
  std::cout << *iptr << std::endl; // derefrencing the pointer reflects the change made in the variable
  *iptr = 50;  // pointer variables can be used to change the value
  std::cout << *iptr << std::endl; // Accessing the changed value

  //pointer arithmetic
  int intArray[] = {15, 30, 45, 60};
  std::cout << intArray[2] << std::endl;
  std::cout << *(intArray + 2) << std::endl; // syntax differs but operation is same, here the element at index 2 is displayed

  //dynamic memory application
  float* fltPtr = new float(50.505); // A float has been created in dynamic memory
  std::cout << *fltPtr << std::endl;
  
  int* intArray1 = new int[10]; // A dynamic array of size 10 has been created
  intArray1[0] = 20;
  std::cout << intArray1[0] << std::endl;
}