#include <iostream>

void swap(int& fir, int& sec){
  int tmp(fir);
  fir = sec;
  sec = tmp; 
}

int main() {
  int a{2011};
  int b{2014};
  std::cout << "a: " << a <<", b: " << b << std::endl;
  swap(a, b); //  Function call
  std::cout << "After swap" << std::endl;
  std::cout << "a: " << a <<", b: " << b << std::endl;
}